
<?php

    $startDate = new DateTime("1981-11-03");
    $endDate = new DateTime("2013-09-04");
       $difference= $startDate->diff($endDate);

   echo "Difference :".$difference->y. "years," .$difference->m. "months," .$difference->d."days";

/*
 Sample Dates : 1981-11-03, 2013-09-04  Expected Result : Difference : 31 years, 10 months, 1 days
 */
?>
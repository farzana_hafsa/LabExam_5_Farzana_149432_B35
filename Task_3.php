<?php
class MyCalculator
{
    public function __construct($num1, $num2)
    {
        $this->num1 = $num1;
        $this->num2 = $num2;

    }

    public function add()
    {
        return $this->num1 + $this->num2;
    }

    public function sub()
    {
        return $this->num1 - $this->num2;
    }

    public function multiply()
    {
        return $this->num1 * $this->num2;
    }

    public function divide()
    {
        return $this->num1 / $this->num2;
    }
}
$mycalc = new MyCalculator( 12, 6);
echo "ADD:".$mycalc-> add()."</br></br>";
echo "Subtract:".$mycalc-> sub()."</br></br>";
echo "Multiply:".$mycalc-> multiply()."</br></br>";
echo "Divide:".$mycalc-> divide()."</br></br>";

/*
 $mycalc = new MyCalculator( 12, 6);
 echo $mycalc- > add(); // Displays 18  echo $mycalc- > multiply(); // Displays 72
 */
?>